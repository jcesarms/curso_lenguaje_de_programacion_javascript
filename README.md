							PRACTICA FINAL

El proyecto es una página web donde podrás buscar y ver películas en línea. También podrás hacer un registro en la página. 
Tiene un diseño amigable y sutil. La página no está terminada, pero ese es el concepto general. 
En cuanto a la estructura del proyecto observamos que tiene una estructura bien definida es decir que hay archivos de .js, .css y .html que le corresponden a cada proposito de la página, como es css para el diseño, html para la estructura y js para sus funcionalidades como es en el caso de la búsqueda de las películas. 

Solución al proyecto final.

Al analizar la pagina y en particular el archivo catalago.js observe que eso me podría ayudar con la búsqueda de las películas. 
Básicamente el código del archivo catalogo.js es el mismo que implemente en la búsqueda, obviamente cambie algunas cosas para poder realizar la función. 
La primera cosa que cambie fue el api_key, esto porque hice una prueba con postman para ver si el api estaba funcionando correctamente, hice algunas peticiones y me marcaba errores, entonces me di la tarea de investigar y se me ocurrió copiar el mismo api_key que se uso en el archivo catalogo.js y como arte de magina las peticiones se hacían correctamente. 
Después de solo cambie algunos parámetros para tener una mejor visibilidad en las películas.
Una cosa que note y que logre solucionar, fue cuando el campo de la búsqueda se quedaba en blanco y le dábamos buscar, por consola se mandaba un error, pero la animación se quedaba en un ciclo y no terminaba, entonces solo hice un “if else” para que cuando el campo este en vacío o blanco mande una alerta y no se ejecute la búsqueda. 
Con esto último se termina mi solución, gracias.
